# README #

## Ludum Dare 34 entry ##

* Theme: Growing
* Game created from scratch in 48 hours by Maarten Engels (maarten@thedreamweb.eu).
* Licensed CC4 BY NC

### About the game ###
Frantic top down shooter where growing your tail grants you power. But beware, as too long a tail might bite you in the...
You will need the power because enemies become exponentially tougher, muhahaha.

### About the entry ###
This game was created using the following tooling: 

* Unity3D
* 3D models: Blender
* 2D images: Pixelmator
* Sound effects: AS3SFXR (http://www.superflashbros.net/as3sfxr/)
* Music: Abundant Music (http://www.abundant-music.com)

Unfortunately, I couldn't add everything I wanted:

* power-ups
* using the tail as a weapon itself
* more enemy types

### This repository ###

* Contains work in progress version so no guarantees it works;
* You can use the assets (scripts, "art", music / noise, ...) as you like within the limits of the License.
* Final delivery in Stable Build folder

### About the author ###
First ever Ludum Dare entry. Main goal: finish the game and submit it!