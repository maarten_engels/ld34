﻿/// <summary>
/// GameManager.cs
/// (c) Maarten Engels, 2015
/// Ludum Dare 34 project
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public GameObject playerPrefab;
	public Transform playerSpawnPosition;
	private int _lives = 4;
	public int Lives {
		get { return _lives; }
		set { _lives = value;
			_lives = Mathf.Max(_lives, 0);
			UpdateLivesText();
		}
	}

	private int _score = 0;
	public int Score {
		get { return _score; }
		set { _score = value;
			UpdateScoreText();
			if (_score > _highScore) {
				_highScore = _score;
				UpdateHighScoreText();
			}
		}
	}

	private int _highScore = 0;
	private bool _gameOver = false;
	private bool _paused = false;

	public int maxLives = 4;
	public float respawnDelay = 3.0f;

	public int numberOfSpawnedEnemies = 0;

	[Header("HUD stuff")]
	public Text scoreText;
	public Text highScoreText;
	public Text livesText;
	public Text respawnTimerText;

	[Header("In Game Menu")]
	public GameObject inGameMenuPanel;
	public Text menuTitleText;
	public Button resumeButton;
	public Button restartButton;
	public Button mainMenuButton;

	void Awake() {
		Instance = this;	
	}

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey("HighScore")) {
			_highScore = PlayerPrefs.GetInt("HighScore");
		}
		ShowInGameMenu(false);
		UpdateHighScoreText();
		Score = 0;
		Lives = maxLives;
		SpawnPlayer();
	}
	
	// Update is called once per frame
	void Update () {
		if (!_gameOver && Input.GetKeyDown(KeyCode.Escape)) {
			if (!_paused) {
				menuTitleText.text = "Paused";
				_paused = true;
				ShowInGameMenu(true);
			} else {
				_paused = false;
				ShowInGameMenu(false);
			}
		}
	}

	void SpawnPlayer() {
		Lives--;
		Instantiate(playerPrefab, playerSpawnPosition.position, Quaternion.identity);

	}

	public void EnemySpawned() {
		numberOfSpawnedEnemies++;
	}

	public void EnemyKilled(Enemy enemy) {
		Score += enemy.scoreValue;
	}

	private void UpdateScoreText() {
		scoreText.text = _score.ToString();
	}

	private void UpdateLivesText() {
		livesText.text = "Lives: "+_lives;
	}
	
	private void UpdateHighScoreText() {
		highScoreText.text = "High: "+_highScore;
	}

	public void PlayerDied() {
		if (_lives > 0) {
			Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();
			foreach (Enemy enemy in enemies) {
				Destroy(enemy.gameObject);
			}
			numberOfSpawnedEnemies = 0;
		StartCoroutine("Respawn");
		} else {
			GameOver();
		}
	}

	IEnumerator Respawn() {
		float t = respawnDelay;
		while (t > 0) {
			t -= Time.deltaTime;
			t = Mathf.Max(t, 0);
			respawnTimerText.text = "Respawning...\n"+t.ToString("F2");
			yield return 0;
		}
		respawnTimerText.text = "";
		SpawnPlayer();
	}

	private void GameOver() {
		Debug.Log("GameOver");
		_gameOver = true;
		menuTitleText.text = "Game Over";
		PlayerPrefs.SetInt("HighScore", _highScore);
		ShowInGameMenu(true);
	}

	#region In Game Menu
	private void ShowInGameMenu(bool visibility) {
		if (visibility) {
			inGameMenuPanel.SetActive(true);
			if (!_gameOver) {
				resumeButton.interactable = true;
			} else {
				resumeButton.interactable = false;
			}
			Time.timeScale = float.Epsilon;
		} else {
			inGameMenuPanel.SetActive(false);
			Time.timeScale = 1.0f;
		}
	}

	public void ResumeButtonClicked() {
		ShowInGameMenu(false);
	}

	public void RestartButtonClicked() {
		Application.LoadLevel(Application.loadedLevel);
	}

	public void MainMenuButtonClicked() {
		Application.LoadLevel("MainMenu");
	}
	#endregion
}
