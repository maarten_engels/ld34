﻿/// <summary>
/// Enemy.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int scoreValue;
	public Text spawnText;

	// Use this for initialization
	void Start () {	
		GameManager.Instance.EnemySpawned();
		scoreValue = Mathf.FloorToInt(Mathf.Pow(2.0f, GameManager.Instance.numberOfSpawnedEnemies));
		Debug.Log("Spawned with scoreValue: "+scoreValue);
		int hp = Mathf.FloorToInt(Mathf.Pow(2.0f, GameManager.Instance.numberOfSpawnedEnemies-1));
		GetComponent<Health>().MaxHealth = hp;

		spawnText = GameObject.Find("SpawnMessageText").GetComponent<Text>();
		spawnText.text = "Just spawned an enemy with "+hp+" hitpoints.";
		Invoke("ResetText", 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ResetText() {
		spawnText.text = "";
	} 
}
