﻿/// <summary>
/// Shoot.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	public GameObject projectilePrefab;
	public float shootRate = 5f;
	public Transform gunMount;

	private float _shootDelay = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_shootDelay -= Time.deltaTime;
		if (Input.GetButton("Fire1")) {
			if (_shootDelay < 0) {
				Instantiate(projectilePrefab, gunMount.position, gunMount.rotation);
				_shootDelay = 1.0f / shootRate;
			}
		}
	}
}
