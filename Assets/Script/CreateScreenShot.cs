﻿using UnityEngine;
using System.Collections;

public class CreateScreenShot : MonoBehaviour {

	private int count = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.P)) {
			count++;
			Application.CaptureScreenshot("Screenshot "+ System.DateTime.Now.ToString("yyyyMd hhmmss ")+ count + ".png", 3);
		}

	}
}
