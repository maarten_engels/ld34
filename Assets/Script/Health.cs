﻿/// <summary>
/// Health.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	private int _maxHealth = 1;
	public int MaxHealth {
		set { _maxHealth = value; 
			_currentHealth = _maxHealth; }
	}
	public GameObject gibOnDeath;


	private int _currentHealth = 1;

	// Use this for initialization
	void Start () {
//		_currentHealth = _maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TakeDamage(int damage) {
		_currentHealth -= damage;
		if (_currentHealth <= 0) {
			Die();
		}
	}

	private void Die() {
		if (gibOnDeath != null) {
			Instantiate(gibOnDeath, transform.position, transform.rotation);
		}
		Enemy enemy = GetComponent<Enemy>();
		if (enemy != null) {
			GameManager.Instance.EnemyKilled(enemy);
		}

		Destroy(gameObject);
	}
}
