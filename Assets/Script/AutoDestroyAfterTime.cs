﻿/// <summary>
/// AutoDestroyAfterTime.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class AutoDestroyAfterTime : MonoBehaviour {

	public float delay = 1.0f;
	// Use this for initialization
	void Start () {
		Destroy(gameObject, delay);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
