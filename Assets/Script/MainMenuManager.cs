﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	public GameObject instructionsPanel;
	public bool deletePlayerPrefs = false;

	// Use this for initialization
	void Start () {
		if (deletePlayerPrefs) {
			PlayerPrefs.DeleteAll();
		}

		if (PlayerPrefs.HasKey("Launched")) {
			InstructionsPanelVisible(false);
		} else {
			InstructionsPanelVisible(true);
			PlayerPrefs.SetString("Launched", "true");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InstructionsPanelVisible(bool visible) {
		if (visible) {
			instructionsPanel.SetActive(true);
		} else {
			instructionsPanel.SetActive(false);
		}
	}

	// Button handlers
	public void OnInstructionsButtonClicked() {
		InstructionsPanelVisible(true);
	}

	public void OnCloseInstructionsButtonClicked() {
		InstructionsPanelVisible(false);
	}

	public void OnStartGame() {
		Application.LoadLevel("Test");
	}
}
