﻿/// <summary>
/// Tail piece.cs
/// (c) Maarten Engels, 2015
/// Ludum Dare 34 project
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class TailPiece : MonoBehaviour {

	public GrowTail owner;
	public Transform linkedObject;
	public float targetDistance = 0.5f;
	public float damping = 1.0f;
	public GameObject gib;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (linkedObject != null) {
		

		Vector3 newPosition	= Vector3.Lerp(transform.position, linkedObject.position - linkedObject.forward * targetDistance, Time.deltaTime * damping);
		newPosition = Bounds.ClampWithinBounds(newPosition);
		
		transform.position = newPosition;

		transform.rotation = Quaternion.Lerp(transform.rotation, linkedObject.rotation, Time.deltaTime * damping);}

	}

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Enemy" && owner != null) {
			owner.Die();
		}
	}

	public void Die() {
		if (gib != null) {
			Instantiate(gib, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}
