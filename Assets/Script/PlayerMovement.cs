﻿/// <summary>
/// Player movement.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed = 1.0f;
	public float rotationSpeed = 360f;
	public LayerMask layerMask;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));


		if (Physics.Raycast(transform.position, transform.up, 0.5f, layerMask)) {
			input.y = Mathf.Min(0, input.y);
		}

		Vector3 targetDirection = new Vector3(input.x, 0, input.y).normalized;

		if (input.sqrMagnitude > 0f) {
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(targetDirection), rotationSpeed * Time.deltaTime);
			Vector3 newPosition = transform.position + transform.forward * speed * Time.deltaTime;

			newPosition = Bounds.ClampWithinBounds(newPosition);

			transform.position = newPosition;
		}
	}
}
