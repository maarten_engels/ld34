﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioMixer audioMixer;

	public Toggle sfxToggle;
	public Toggle musicToggle;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey("SFXEnabled")) {
			if (PlayerPrefs.GetInt("SFXEnabled") == 1) {
				SetSFXEnabled(true);
			} else {
				SetSFXEnabled(false);
			}
		}
		if (PlayerPrefs.HasKey("MusicEnabled")) {
			if (PlayerPrefs.GetInt("MusicEnabled") == 1) {
				SetMusicEnabled(true);
			} else {
				SetMusicEnabled(false);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// enable/disable audio helper functions
	public void SetSFXEnabled(bool enabled) {
		if (enabled) {
			audioMixer.SetFloat("SFXVolume", 0.0f);
			PlayerPrefs.SetInt("SFXEnabled", 1);
			sfxToggle.isOn = true;
		} else {
			audioMixer.SetFloat("SFXVolume", -80.0f);
			PlayerPrefs.SetInt("SFXEnabled", 0);
			sfxToggle.isOn = false;
		}
	}

	public void SetMusicEnabled(bool enabled) {
		if (enabled) {
			audioMixer.SetFloat("MusicVolume", 0.0f);
			PlayerPrefs.SetInt("MusicEnabled", 1);
			musicToggle.isOn = true;
		} else {
			audioMixer.SetFloat("MusicVolume", -80.0f);
			PlayerPrefs.SetInt("MusicEnabled", 0);
			musicToggle.isOn = false;
		}
	}


}
