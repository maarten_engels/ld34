﻿/// <summary>
/// Spawn object.cs
/// (c) Maarten Engels, 2015
/// Ludum Dare 34 project
/// </summary>
using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour {

	public GameObject[] prefabs;
	public Transform[] spawnPositions;

	public float minSpawnTime = 1.0f;
	public float maxSpawnTime = 2.0f;

	// Use this for initialization
	void Start () {
		float delay = Random.Range(minSpawnTime, maxSpawnTime);
		Invoke("Spawn", delay);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Spawn() {
		int index = Random.Range(0, prefabs.Length);
		Vector3 spawnPosition = spawnPositions[Random.Range(0, spawnPositions.Length)].position;

		Instantiate(prefabs[index], spawnPosition, Quaternion.identity);


		float delay = Random.Range(minSpawnTime, maxSpawnTime);
		Invoke("Spawn", delay);
	}
}
