﻿using UnityEngine;
using System.Collections;

public class DamageOnTrigger : MonoBehaviour {

	public GrowTail owner;
	public GameObject gib;

	// Use this for initialization
	void Start () {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		owner = player.GetComponent<GrowTail>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col) {
		Health h = col.GetComponent<Health>();
		if (h != null) {
			h.TakeDamage(owner.DamageAmount);
		}
		Die();
	}

	void Die() {
		if (gib != null) {
			Instantiate(gib, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}
