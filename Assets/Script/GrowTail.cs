﻿/// <summary>
/// Grow tail.cs
/// (c) Maarten Engels, 2015
/// Ludum Dare 34 project
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GrowTail : MonoBehaviour {

	public List<TailPiece> tailPieces;
	public GameObject gib;
	public Text currentDamageText;

	// Use this for initialization
	void Start () {
		tailPieces = new List<TailPiece>();
		GameObject go = GameObject.Find("CurrentDamageText"); 
		currentDamageText = go.GetComponent<Text>();
		currentDamageText.text = "Current damage: "+DamageAmount;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int DamageAmount {
		get { 
			return Mathf.FloorToInt(Mathf.Pow(2.0f, tailPieces.Count));
		}
	}

	void OnTriggerEnter(Collider col) {
		TailPiece tailPiece = col.GetComponent<TailPiece>();
		if (tailPiece != null && tailPiece.owner == null) {
//			Debug.Log("We picked up a new tail piece");
			tailPiece.owner = this;
			if (tailPieces.Count > 0) {
				tailPiece.linkedObject = tailPieces[tailPieces.Count-1].transform;
			} else {
				tailPiece.linkedObject = transform;
			}

			tailPieces.Add(tailPiece);
			currentDamageText.text = "Current damage: "+DamageAmount;

		}

		Enemy enemy = col.GetComponent<Enemy>();
		if (enemy != null) {
			Die();
		}
	}

	public void Die() {
		StartCoroutine("DieEffect");
	}

	IEnumerator DieEffect() {
		while (tailPieces.Count > 0) {
			tailPieces[tailPieces.Count-1].Die();
			tailPieces.RemoveAt(tailPieces.Count - 1);
			yield return new WaitForSeconds(0.1f);
		}
		if (gib != null) {
			Instantiate(gib, transform.position, Quaternion.identity);
		}
		GameManager.Instance.PlayerDied();
		Destroy(gameObject);
	}
}
