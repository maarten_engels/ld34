﻿/// <summary>
/// Enemy AI.cs
/// (c) Maarten Engels, 2015
/// Ludum Dare 34 project
/// </summary>
using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	public float speed = 4;
	public float rotationSpeed = 90;
	public Transform target;

	// Use this for initialization
	void Start () {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if (player != null) {
			target = player.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			Vector3 direction = target.position - transform.position;
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);

			Vector3 newPosition = transform.position + transform.forward * speed * Time.deltaTime;

			newPosition = Bounds.ClampWithinBounds(newPosition);

			transform.position = newPosition;
		}
	}
}
