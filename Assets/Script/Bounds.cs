﻿/// <summary>
/// Bounds.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class Bounds {

	public const float HEIGHT = 7.75f;
	public const float WIDTH = 17.75f;

	public static Vector3 ClampWithinBounds(Vector3 position) {

		Vector3 newPosition = position;

		if (newPosition.x < -Bounds.WIDTH) {
			newPosition.x = -Bounds.WIDTH;
		}

		if (newPosition.x > Bounds.WIDTH) {
			newPosition.x = Bounds.WIDTH;
		}

		if (newPosition.z < -Bounds.HEIGHT) {
			newPosition.z = -Bounds.HEIGHT;
		}

		if (newPosition.z > Bounds.HEIGHT) {
			newPosition.z = Bounds.HEIGHT;
		}

		return newPosition;
	}

	public static Vector3 ClampWithinBoundsPercentage(Vector3 position, float percentage) {

		Vector3 newPosition = position;

		if (newPosition.x < -Bounds.WIDTH * percentage) {
			newPosition.x = -Bounds.WIDTH * percentage;
		}

		if (newPosition.x > Bounds.WIDTH * percentage) {
			newPosition.x = Bounds.WIDTH * percentage;
		}

		if (newPosition.z < -Bounds.HEIGHT * percentage) {
			newPosition.z = -Bounds.HEIGHT * percentage;
		}

		if (newPosition.z > Bounds.HEIGHT * percentage) {
			newPosition.z = Bounds.HEIGHT * percentage;
		}

		return newPosition;
	}

	public Bounds() {
		
	}

}
