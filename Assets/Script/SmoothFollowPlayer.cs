﻿/// <summary>
/// Smooth follow player.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 34
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
using UnityEngine;
using System.Collections;

public class SmoothFollowPlayer : MonoBehaviour {

	public float damping = 4f;

	private Transform _player;
	private Vector3 _offset;
	private Vector3 _startPosition;

	// Use this for initialization
	void Start () {
		_offset = transform.position - GameManager.Instance.playerSpawnPosition.position ;
	}
	
	void LateUpdate () {
		if (_player != null ) {
			Vector3 targetPosition = _player.position + _offset;
			Vector3 newPosition = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * damping);

			newPosition = Bounds.ClampWithinBoundsPercentage(newPosition, 0.6f);

			transform.position = newPosition;
		} else {
			GameObject go = GameObject.FindGameObjectWithTag("Player");
			if (go != null) {
				_player = go.transform;
			}
		}
	}
}
